package model;

import java.util.ArrayList;
import java.util.List;

public class Word {
	
	private String word;
	private boolean found;
	private List<Integer> pageFound;
	
	public Word() {
	}
	public Word(String word) {
		super();
		this.word = word;
		//found = false;
		pageFound =new ArrayList<Integer>();
	}
	public Word(String word, boolean found, List<Integer> pageFound) {
		super();
		this.word = word;
		this.found = found;
		this.pageFound = pageFound;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public boolean isFound() {
		return found;
	}
	public void setFound(boolean found) {
		this.found = found;
	}
	public List<Integer> getPageFound() {
		return pageFound;
	}
	public void setPageFound(List<Integer> pageFound) {
		this.pageFound = pageFound;
	}
	@Override
	public String toString() {
		return "Word [word=" + word + ", found=" + found + ", pageFound=" + pageFound + "]";
	}
}
