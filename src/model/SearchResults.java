package model;

import java.util.ArrayList;
import java.util.List;

//will be used to collect the search results of the previous method 
//(e.g Word : �Foo�, Found: Yes ,Page Found: p.9)
public class SearchResults {
	
	
	private List<Word> list;

	
	public SearchResults() {
		list =new ArrayList<>();
	}
	
	public SearchResults(List<Word> list) {
		super();
		this.list = list;
	}

	public List<Word> getList() {
		return list;
	}

	public void setList(List<Word> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "SearchResults [list=" + list + "]";
	}


}
