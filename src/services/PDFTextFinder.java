package services;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import model.SearchResults;
import model.Word;

/**
 * 
 * @author ΒΑΣΙΛΙΚΗ
 *
 */
public class PDFTextFinder {

	
	/**
	 * read pdf documents
	 * try to find the desired word(text)
	 * @param fileName
	 * @param text
	 * @return
	 * @throws IOException
	 */
	public SearchResults findTextInPDFDocument(String fileName, List<String> text) throws  IOException{
		
		SearchResults searchResults  = new SearchResults();
		
		List<Word> listOfWord = searchResults.getList();
		
		 try (PDDocument document = PDDocument.load(new File(fileName))) {
		      //Instantiating Splitter class
		      Splitter splitter = new Splitter();

		      //splitting the pages of a PDF document
		      List<PDDocument> Pages = splitter.split(document);

		      //Creating an iterator 
		     // Iterator<PDDocument> pagesIterator = Pages.listIterator();

		      //gemizw thn listOfWord: 
		      for(int m=0 ; m<text.size() ; m++ ) {
		    	  
		    	  listOfWord.add(new Word(text.get(m)));	
		      }
		      
		      //Saving each page as an individual document
		     // int i = 1;
		      for(int i= 0 ; i< Pages.size() ; i++) {
		         PDDocument currentPage = Pages.get(i);
		         currentPage.getClass();

		            if (!currentPage.isEncrypted()) {
					
		                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
		                stripper.setSortByPosition(true);

		                PDFTextStripper tStripper = new PDFTextStripper();

		                String pdfFileInText = tStripper.getText(currentPage);
		                //System.out.println("Text:" + st);

		                for(int k=0 ; k<listOfWord.size() ; k++) {
		                	//pdfFileInText.contains(text.get(k));
		                	
		                	if(pdfFileInText.contains(text.get(k))) {  //edw prepei na to kannw true alla mono mia fora
		                		
		                		if(!listOfWord.get(k).isFound()) {
		                			listOfWord.get(k).setFound(true);
		                		}
		                		
		                		listOfWord.get(k).getPageFound().add(new Integer(i+1));
		                	}
		                }
		            }
		      }  
		 }
		 
		 return searchResults;
	}
}
